package com.exposit.console;

import com.exposit.models.CentralLibrary;
import com.exposit.models.Command;

public class ConsoleLibrary {

	public static void main(String[] args) {

		CentralLibrary centralLibrary = new CentralLibrary();
		centralLibrary.load();
		while (true) {
			Command commandToExecute = new Command();
			if (!commandToExecute.isExit()){
				centralLibrary.executeCommand(commandToExecute);
			}
			else break;
		}
	}
}

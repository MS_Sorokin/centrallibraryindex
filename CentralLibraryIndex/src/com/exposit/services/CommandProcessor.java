package com.exposit.services;

import java.util.ArrayList;
import java.util.List;

import com.exposit.models.Command;
import com.exposit.models.CommandAttribute;
import com.exposit.models.Library;
import com.exposit.models.Result;
import com.exposit.models.ResultType;

public class CommandProcessor {

	private Command command;

	public CommandProcessor(Command commandToExecute) {

		this.command = commandToExecute;
	}

	public List<Result> executeCommand(List<Library> libraries) {

		List<Result> results = new ArrayList<Result>();
		Result result = null;
		if (command.isValid()) {
			switch (command.getCommandType()) {
			case FIND: {
				for (Library library : libraries) {
					results.addAll(library.findBook(
							command.valueOfAttribute(CommandAttribute.author),
							command.valueOfAttribute(CommandAttribute.name)));
				}
				break;
			}

			case ORDER: {
				for (Library library : libraries) {
					result = library.orderBook(Integer.parseInt(command
							.valueOfAttribute(CommandAttribute.id)), command
							.valueOfAttribute(CommandAttribute.abonent));
					if (result != null) {
						results.add(result);
						break;
					}
				}
				break;
			}

			case RETURN: {
				for (Library library : libraries) {
					result = library.returnBook(Integer.parseInt(command
							.valueOfAttribute(CommandAttribute.id)));
					if (result != null) {
						results.add(result);
						break;
					}
				}
				break;
			}

			case EXIT: {
				break;
			}
			}
		} else {
			result = new Result(ResultType.SYNTAXERROR);
			results.add(result);
		}
		return results;
	}

	public void toConsole(List<Result> results) {
		if (results.size() == 0) {
			results.add(new Result(ResultType.NOTFOUND));
		}

		for (Result result : results) {
			result.toConsole();
		}
	}
}

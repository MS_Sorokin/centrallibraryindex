package com.exposit.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.exposit.models.Book;
import com.exposit.models.Document;
import com.exposit.models.ILibrary;
import com.exposit.models.Result;
import com.exposit.models.ResultType;

public class DocumentProcessor implements ILibrary {
	private Document document;
	private Result result;
	private SimpleDateFormat dateFormat;
	private List<Result> resultBooks;

	public DocumentProcessor(Document document) {
		this.document = document;
		this.dateFormat = new SimpleDateFormat("yyyy.MM.dd");
	}

	@Override
	public List<Result> findBook(String author, String name) {
		resultBooks = new ArrayList<Result>();
		result = new Result();
		for (Book book : document.getBooks()) {
			if ((book.getAuthor().contains(author) && name == null)
					|| (book.getAuthor().contains(author) && book.getName().equals(name))) {
				if (book.getIssued() == null) {
					result.setResultType(ResultType.FOUND);
					result.setResultParams("id=" + book.getId() + " lib=" + document.getLibraryName());
				} else {
					result.setResultType(ResultType.FOUNDMISSING);
					result.setResultParams("id=" + book.getId() + " lib=" + document.getLibraryName() + " issued="
							+ dateFormat.format(book.getIssued()));
				}
				resultBooks.add(result);
			}
		}
		return resultBooks;
	}

	@Override
	public Result orderBook(Integer id, String abonent) {
		result = null;
		for (Book book : document.getBooks()) {
			if (book.getId() == id) {
				result = new Result();
				if (book.getIssuedTo() == null) {
					book.setIssuedTo(abonent);
					book.setIssued(new Date());
					document.saveChanges();
					result.setResultType(ResultType.OK);
					result.setResultParams(
							"abonent=" + book.getIssuedTo() + " date=" + dateFormat.format(book.getIssued()));
				} else if (book.getIssuedTo() != null) {
					result.setResultType(ResultType.RESERVED);
					result.setResultParams(
							"abonent=" + book.getIssuedTo() + " date=" + dateFormat.format(book.getIssued()));
				}
			}
		}

		return result;
	}

	@Override
	public Result returnBook(Integer id) {
		result = null;
		for (Book book : document.getBooks()) {
			if (book.getId() == id) {
				result = new Result();
				if (book.getIssuedTo() != null) {
					result.setResultType(ResultType.OK);
					result.setResultParams("abonent=" + book.getIssuedTo());
					book.setIssuedTo(null);
					book.setIssued(null);
					document.saveChanges();
				} else {
					result.setResultType(ResultType.ALREADYRETURNED);
				}
			}
		}
		return result;
	}

	public Book readOneBook() {
		Object[] newBook = document.readObject();
		if (newBook != null) {
			int id = Integer.parseInt((String) newBook[0]);
			String author = (String) newBook[1];
			String name = (String) newBook[2];
			Date issued = null;
			try {
				issued = dateFormat.parse((String) newBook[3]);
			} catch (ParseException e) {
				issued = null;
				String issuedTo = null;
				return new Book(id, author, name, issued, issuedTo);
			}
			String issuedTo = (String) newBook[4];
			return new Book(id, author, name, issued, issuedTo);
		} else
			return null;
	}
}

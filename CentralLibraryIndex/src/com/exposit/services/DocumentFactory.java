package com.exposit.services;

import com.exposit.models.CSVDocument;
import com.exposit.models.Document;
import com.exposit.models.TextDocument;

public class DocumentFactory {

	private Document document;
	private String filePath;
	
	
	public DocumentFactory(String filePath){
		this.filePath=filePath;
	}
	
	public Document createDocument(){
		if (filePath.contains("csv")){
			document=new CSVDocument(filePath);
		}
		else if (filePath.contains("properties")){
			document=new TextDocument(filePath);
		}
		return document;
	}
}

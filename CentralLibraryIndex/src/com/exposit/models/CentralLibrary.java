package com.exposit.models;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.exposit.services.CommandProcessor;

public class CentralLibrary {

	private List<Library> libraries;
	private final String LIBRARIES_FOLDER = "Libraries";

	public CentralLibrary() {
		libraries = new ArrayList<Library>();
	}

	public void load() {
		File centralLibraryFolder = new File(LIBRARIES_FOLDER);
		for (String oneLibraryName : centralLibraryFolder.list()) {
			String includedLibraryPath = LIBRARIES_FOLDER.concat("\\").concat(
					oneLibraryName);
			Library newLibrary = new Library(includedLibraryPath);
			newLibrary.load();
			libraries.add(newLibrary);
		}
	}

	public void executeCommand(Command command) {
			CommandProcessor commandProcessor = new CommandProcessor(command);
			commandProcessor.toConsole(commandProcessor.executeCommand(libraries));
	}
}

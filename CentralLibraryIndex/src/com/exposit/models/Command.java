package com.exposit.models;

import java.util.HashMap;
import java.util.Scanner;

public class Command {
	private String commandString;
	private Scanner scanner;
	private CommandType commandType;
	private HashMap<String, String> attributes;

	public Command() {
		scanner = new Scanner(System.in);
		this.commandString = scanner.nextLine();
		attributes = new HashMap<String, String>();
		this.parse();
	}

	public void parse() {
		String[] partsOfCommand = commandString.split(" ");
		try {
			this.commandType = CommandType.valueOf(partsOfCommand[0]);
			for (int i = 1; i < partsOfCommand.length; i++) {
				String key = partsOfCommand[i].split("=")[0];
				String value = partsOfCommand[i].split("=")[1];
				attributes.put(key, value);
			}
		} catch (Exception e) {
			this.commandType = null;
		}

	}

	public boolean isValid() {

		boolean result = true;
		if (commandType != null) {
			switch (commandType) {
			case FIND: {
				if (attributes.size() == 1) {
					result = result && this.hasAttribute(CommandAttribute.author);
				} else
					result = result && this.hasAttribute(CommandAttribute.author)
							&& this.hasAttribute(CommandAttribute.name);
				break;
			}
			case ORDER: {
				result = result && this.hasAttribute(CommandAttribute.id)
						&& this.hasAttribute(CommandAttribute.abonent);
				break;
			}
			case RETURN: {
				result = result && this.hasAttribute(CommandAttribute.id);
				break;
			}
			case EXIT: {
				// code for exit!;
				break;
			}
			}
			return result;
		} else
			return false;
	}

	public CommandType getCommandType() {

		return commandType;
	}

	public boolean hasAttribute(CommandAttribute attribute) {

		if (attributes.containsKey(attribute.toString())) {
			return true;
		} else
			return false;
	}

	public String valueOfAttribute(CommandAttribute attribute) {

		return attributes.get(attribute.toString());
	}

	public boolean isExit() {
		if (commandType == CommandType.EXIT)
			return true;
		else
			return false;
	}

}

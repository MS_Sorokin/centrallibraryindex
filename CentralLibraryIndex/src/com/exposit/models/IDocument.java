package com.exposit.models;

public interface IDocument {
	public void loadBooks();
	public void saveChanges();

}

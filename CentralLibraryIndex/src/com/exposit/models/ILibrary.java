package com.exposit.models;

import java.util.List;

public interface ILibrary {

	public List<Result> findBook(String author,String name);
	public Result orderBook(Integer id,String abonent);
	public Result returnBook(Integer id);
}

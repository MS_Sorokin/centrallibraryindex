package com.exposit.models;

public enum CommandType {

	FIND, ORDER, RETURN, EXIT
}

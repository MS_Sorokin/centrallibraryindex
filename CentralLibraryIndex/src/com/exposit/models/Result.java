package com.exposit.models;

public class Result {

	private ResultType resultType;
	private String resultParams;
	
	public Result(){
		
	}
	
	public Result(ResultType resultType) {
		this.resultType=resultType;
	}
	public Result(ResultType resultType,String resultParams){
		this(resultType);
		this.resultParams=resultParams;
	}

	public void toConsole() {
		if (resultParams!=null){
			System.out.println(resultType.toString()+" "+resultParams);	
		}
		else System.out.println(resultType.toString());
		
	}

	public ResultType getResultType() {
		return resultType;
	}

	public void setResultType(ResultType resultType) {
		this.resultType = resultType;
	}
	public void setResultParams(String resultParams) {
		this.resultParams = resultParams;
	}
	
	

}

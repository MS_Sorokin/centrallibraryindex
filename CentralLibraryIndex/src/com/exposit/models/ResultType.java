package com.exposit.models;

public enum ResultType {
	FOUND, FOUNDMISSING, NOTFOUND, OK, RESERVED, ALREADYRETURNED, SYNTAXERROR
}

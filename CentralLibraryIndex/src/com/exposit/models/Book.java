package com.exposit.models;

import java.util.Date;

public class Book {

	private int id;
	private String author;
	private String name;
	private Date issued;
	private String issuedTo;

	public Book() {

	}

	public Book(int id, String author, String name, Date issued, String issuedTo) {

		this.id = id;
		this.author = author;
		this.name = name;
		this.issued = issued;
		this.issuedTo = issuedTo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getIssued() {
		return issued;
	}

	public void setIssued(Date issued) {
		this.issued = issued;
	}

	public String getIssuedTo() {
		return issuedTo;
	}

	public void setIssuedTo(String issuedTo) {
		this.issuedTo = issuedTo;
	}

}

package com.exposit.models;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import com.exposit.services.DocumentProcessor;

public class TextDocument extends Document {

	private InputStream stream;
	private Properties properties = new Properties();
	private final String ID_BOOK = "Index";
	private final String BOOK_AUTHOR_SURNAME = "Author";
	private final String BOOK_NAME = "Name";
	private final String ORDER_DATE = "Issued";
	private final String ORDER_SURNAME = "IssuedTo";
	private DocumentProcessor processor;
	private OutputStream output;

	public TextDocument(String filePath) {
		super(filePath);
		try {
			stream = new FileInputStream(filePath);
			this.properties.load(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		processor = new DocumentProcessor(this);
	}

	@Override
	public String[] readObject() {

		String id = this.properties.getProperty(ID_BOOK);
		String author = this.properties.getProperty(BOOK_AUTHOR_SURNAME);
		String name = this.properties.getProperty(BOOK_NAME);
		String issued = this.properties.getProperty(ORDER_DATE);
		String issuedTo = this.properties.getProperty(ORDER_SURNAME);
		return new String[] { id, author, name, issued, issuedTo };
	}

	public void loadBooks() {
		this.books.add(processor.readOneBook());

	}

	public void saveChanges() {
		Book bookInFile = this.getBooks().get(0);
		try {
			output=new FileOutputStream(new File(this.filePath));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		try {
			properties.setProperty(ID_BOOK, String.valueOf(bookInFile.getId()));
			properties.setProperty(BOOK_AUTHOR_SURNAME, bookInFile.getAuthor());
			properties.setProperty(BOOK_NAME, bookInFile.getName());
			if (bookInFile.getIssued()!=null){
				properties.setProperty(ORDER_DATE,
						dataFormat.format(bookInFile.getIssued()));
				properties.setProperty(ORDER_SURNAME, bookInFile.getIssuedTo());
			}
			else {
				properties.setProperty(ORDER_DATE,
						"");
				properties.setProperty(ORDER_SURNAME, "");
			}
			
			
			properties.store(output, null);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

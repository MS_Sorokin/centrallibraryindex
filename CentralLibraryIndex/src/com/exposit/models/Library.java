package com.exposit.models;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.exposit.services.DocumentFactory;
import com.exposit.services.DocumentProcessor;

public class Library implements ILibrary {

	private List<Document> documents;
	private String name;
	private String libraryPath;
	private DocumentProcessor documentProcessor;

	public Library(String libraryPath) {

		this.libraryPath = libraryPath;
		File file = new File(libraryPath);
		this.name = file.getName().split("_")[1];
		documents = new ArrayList<Document>();
	}

	public void load() {

		File libraryDirectory = new File(libraryPath);
		String[] libraryDocuments = libraryDirectory.list();
		for (String oneDocument : libraryDocuments) {
			DocumentFactory documentCreator = new DocumentFactory(libraryPath.concat("\\").concat(oneDocument));
			Document currentDocument = documentCreator.createDocument();
			currentDocument.loadBooks();
			documents.add(currentDocument);
		}
	}

	public String getName() {
		return name;
	}

	public String getLibraryPath() {
		return libraryPath;
	}

	@Override
	public List<Result> findBook(String author, String name) {
		List<Result> results = new ArrayList<Result>();
		for (Document document : documents) {
			documentProcessor = new DocumentProcessor(document);
			results.addAll(documentProcessor.findBook(author, name));
		}

		return results;
	}

	@Override
	public Result orderBook(Integer id, String abonent) {
		Result result = null;
		for (Document document : documents) {
			documentProcessor = new DocumentProcessor(document);
			result = documentProcessor.orderBook(id, abonent);
			if (result != null) {
				break;
			}
		}
		return result;

	}

	@Override
	public Result returnBook(Integer id) {
		Result result = null;
		for (Document document : documents) {
			documentProcessor = new DocumentProcessor(document);
			result = documentProcessor.returnBook(id);
			if (result != null) {
				break;
			}
		}
		return result;

	}
}

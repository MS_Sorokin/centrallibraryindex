package com.exposit.models;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.exposit.services.DocumentProcessor;

public abstract class Document implements IDocument {

	protected String filePath;
	protected DocumentType documentType;
	protected ArrayList<Book> books = new ArrayList<Book>();
	protected final DateFormat dataFormat = new SimpleDateFormat("yyyy.MM.dd");

	public ArrayList<Book> getBooks() {
		return books;
	}

	public void setBooks(ArrayList<Book> books) {
		this.books = books;
	}

	public Document(String filePath) {
		this.filePath = filePath;
		this.initializeDocumentType();
	}

	public abstract String[] readObject();

	public DocumentType getDocumentType() {
		return this.documentType;
	}

	private void initializeDocumentType() {
		if (filePath.contains("csv")) {
			this.documentType = DocumentType.CSV;
		} else if (filePath.contains("properties")) {
			this.documentType = DocumentType.TEXT;
		}
	}

	public String getLibraryName() {

		return filePath.replace("\\", "/").split("/")[1].split("_")[1];
	}

	public String getPath() {
		return this.filePath;
	}

}

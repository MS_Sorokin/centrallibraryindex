package com.exposit.models;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.exposit.services.DocumentProcessor;

public class CSVDocument extends Document {

	private Scanner scanner;
	private FileWriter writer;
	private DocumentProcessor processor;

	public CSVDocument(String filePath) {
		super(filePath);
			try {
				this.scanner = new Scanner(new File(filePath));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		processor=new DocumentProcessor(this);
	}

	@Override
	public String[] readObject() {
		if (scanner.hasNext()) {
			return scanner.nextLine().split(",");
		}
		return null;
	}

	@Override
	public void loadBooks() {
		
		boolean flag = true;
		while (flag) {
			Book newBook = processor.readOneBook();
			if (newBook != null) {
				books.add(newBook);
			} else
				flag = false;
		}
	}

	public void saveChanges() {
		try {
			writer=new FileWriter(new File(this.filePath));
			for (Book book : this.getBooks()) {
				writer.append(convertBookToString(book) + "\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String convertBookToString(Book currentBook) {
		String temp = null;
		if (currentBook.getIssued() == null) {
			temp = currentBook.getId() + "," + currentBook.getAuthor() + ","
					+ currentBook.getName() + ",, ";
		} else {
			temp = currentBook.getId() + "," + currentBook.getAuthor() + ","
					+ currentBook.getName() + ","
					+ this.dataFormat.format(currentBook.getIssued()) + ","
					+ currentBook.getIssuedTo();
		}
		return temp;
	}
}
